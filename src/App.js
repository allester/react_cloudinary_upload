import React, { useState } from 'react'
import axios from 'axios'
import { Image } from 'cloudinary-react'
import './App.css'

import { cloudinary_config } from './config/cloudinary'

function App() {

  const [selectedImage, setSelectedImage] = useState("")
  const [uploadedImage, setUploadedImage] = useState("")

  const cloudinary_cloud_name = cloudinary_config.CLOUDINARY_CLOUD_NAME
  const cloudinary_upload_preset = cloudinary_config.CLOUDINARY_UPLOAD_PRESET

  const uploadImage = (files) => {
    console.log(files[0])

    const formData = new FormData()
    formData.append('file', selectedImage)
    formData.append('upload_preset', cloudinary_upload_preset)

    axios.post(`https://api.cloudinary.com/v1_1/${cloudinary_cloud_name}/image/upload`, formData)
    .then((response) => {
      // Retrieve image URL from response and save to database
      const imageUrl = response.data.secure_url
      setUploadedImage(imageUrl)
    })
    .catch((error) => {
      console.log(error)
    })

  }

  return (
    <div>
      <input 
        type="file" 
        onChange={(e) => {
          setSelectedImage(e.target.files[0])
        }} 
      />

      <button type="submit" onClick={uploadImage}>Upload Image</button>

      <Image 
        style={{width: 200}}
        cloudName={cloudinary_cloud_name}  
        publicId={uploadedImage}  
      />
    </div>
  );
}

export default App;
